from tache import Tache

class Ordonnancement:

    def __init__(self, taches, marges_totales, taches_critiques, chemins_critiques, sousgraphes_critiques,
                 marges_libres):
        self.taches = Tache(taches)
        self.marges_totales = marges_totales
        self.taches_critiques = taches_critiques
        self.chemins_critiques = chemins_critiques
        self.sousgraphes_critiques = sousgraphes_critiques
        self.marges_libres = marges_libres

    def calcul_plus_long_chemin(self):
        n = len(self.taches)
        for i in range(n):
            self.taches[i].date_debut_au_plus_tot = -1
        self.taches[0].date_debut_au_plus_tot = 0
        nbc = 1
        while nbc < len(n):
            for i in range(n):
                for j in range(i):
                    if self.taches[i].date_debut_au_plus_tot == -1 and self.taches[j].date_debut_au_plus_tot > -1 :
                        self.taches[i].date_debut_au_plus_tot = max(0, 0)
                        nbc += 1

    def calcul_date_plus_tard(self):
        n = len(self.taches)
        for i in range(n):
            self.taches[i].date_debut_au_plus_tard = float("inf")
        self.taches[n+1].date_debut_au_plus_tard = self.taches[n+1].date_debut_au_plus_tot
        nbc = 1
        while nbc < len(n):
            for i in range(n):
                for j in range(i):
                    if self.taches[i].date_debut_au_plus_tard == float("inf") and self.taches[i].date_debut_au_plus_tard < float("inf") :
                        self.taches[i].date_debut_au_plus_tard = min(0, 0)
                        nbc += 1

    def calcul_marge_totale(self):
        n = len(self.taches)
        for i in range(n):
            self.marges_totales[i] = self.taches[i].date_debut_au_plus_tard - self.taches[i].date_debut_au_plus_tot
