from math import *
from random import *
from pulp import *

problem = LpProblem("PLC", LpMaximize)


# Nombre de systèmes
p = 2

# Nombre de variables de décision
n = 0

# nombre de contraintes ( <n )
m = 0

# Valeurs de c ( [0,500] )
vecteur_C = []

# Valeurs de A ( [0,100] )
vecteurs_A = []

# Valeurs de B ( [25,250] )
vecteur_B = []


def generate_values(rand_min, rand_max, nbr, nb_iter=1):
    vector = []
    values = []
    for i in range(0, nb_iter):
        for j in range(0, nbr):
            values.append(randint(rand_min, rand_max))
        vector.append(values)
    return (vector[0], vector)[len(vector) > 1]


for i in range(p):
    # Initialisation des variables
    #n = randint(1000, 100000)
    n = 1000
    #m = randint(50, 1000)
    m = 50
    vecteur_C = generate_values(0, 500, n)
    vecteur_B = generate_values(25, 250, m)
    vecteurs_A = generate_values(0, 100, n, m)
    nom_variables = []

    # Nom des variables
    for j in range(n):
        nom_variables.append("x" + str(j))

    # Affichage debug
    print("Systeme numéro " + str(i))
    print("Nombre de variables : " + str(n))
    print("Nombre de contraintes : " + str(m))

    # Affichage des vecteurs pour debug NE PAS DECOMMENTER
    # for j in range(len(vecteur_B)):
    #     print("Vecteur B : " + str(vecteur_B[j]))
    #     print("Vecteur A : " + str(vecteurs_A[j]))

    variables_decision = LpVariable.dicts("varDict", nom_variables)
    #problem += (lambda x: [variables_decision[k]*vecteur_C[k] for k in range(len(vecteurs_A))])
    for j in range(m):
        a = ([variables_decision[k]*vecteurs_A[k] for k in range(len(vecteurs_A))])
        problem += a <= vecteur_B[j]

    print(problem)









