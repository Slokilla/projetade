class Tache:

    def __init__(self, numero, date_debut_au_plus_tot, date_debut_au_plus_tard, date_fin_au_plus_tot,
                 date_fin_au_plus_tard):
        self.numero = numero
        self.date_debut_au_plus_tot = date_debut_au_plus_tot
        self.date_debut_au_plus_tard = date_debut_au_plus_tard
        self.date_fin_au_plus_tot = date_fin_au_plus_tot
        self.date_fin_au_plus_tard = date_fin_au_plus_tard
